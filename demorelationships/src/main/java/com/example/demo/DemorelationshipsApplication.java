package com.example.demo;

import org.springframework.boot.CommandLineRunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.example")
@EnableJpaRepositories
public class DemorelationshipsApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(DemorelationshipsApplication.class, args);
	}

}
