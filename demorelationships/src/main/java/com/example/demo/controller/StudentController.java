package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Laptop;
import com.example.demo.model.Student;
import com.example.demo.service.StudentService;

@RestController
public class StudentController {
	@Autowired
	private StudentService studentService;

	@GetMapping(value = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Student>> getDetails() {
		List<Student> product = studentService.getAllStudents();

		return ResponseEntity.status(200).body(product);
	}

	@PostMapping(value = "/students", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Student> saveData(@RequestBody Student student) {
		Student p = studentService.saveStudent(student);
		return ResponseEntity.status(201).body(p);
	}

	@PostMapping(value = "/laptops", consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<Laptop> saveLaptop(@RequestBody Laptop laptop) {
		
		Laptop l = studentService.saveLaptop(laptop);
		
		return ResponseEntity.status(201).body(l);

	}
	

}
