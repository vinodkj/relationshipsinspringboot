package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Laptop;
import com.example.demo.model.Student;
import com.example.demo.repository.LaptopRepository;
import com.example.demo.repository.StudentRepository;

@Service
public class StudentService 
{
	@Autowired
	private StudentRepository studentRepository;
	
	@Autowired
	private LaptopRepository laptopRepository;
	
	public List<Student> getAllStudents()
	{
		return studentRepository.findAll();
	}
	public Student saveStudent(Student student)
	{
		return studentRepository.save(student);
	}
	public Laptop saveLaptop(Laptop laptop)
	{
		return laptopRepository.save(laptop);
	}
	

}
