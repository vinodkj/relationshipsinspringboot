package studentcourses;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller
{
	@Autowired
	private Service service;
	
	
	@PostMapping(value="/addcourse")
	public ResponseEntity< Courses> saveCourse(@RequestBody Courses course)
	{
		Courses cou=service.saveCouse(course);
		
		return ResponseEntity.status(200).body(cou);
	}
	

}
