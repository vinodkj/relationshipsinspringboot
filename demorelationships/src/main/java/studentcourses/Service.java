package studentcourses;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service
{
	List topicd=new ArrayList();
	@Autowired
	private CourseRepository courseRepository;
	@Autowired
	private TopicRepository topicRepository;
	
	public Courses saveCouse(Courses course)
	{
		return courseRepository.save(course);
		
	}
	public List<Courses> getAllCourses()
	{
		return courseRepository.findAll();
	
	}
	public Topics saveTopic(Topics topic)
	{
		return topicRepository.save(topic);
		
	}
	public List<Topics> getAllTopics()
	{
		return topicRepository.findAll();
	
	}
	
	

}
